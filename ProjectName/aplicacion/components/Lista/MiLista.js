/*Example to add or remove flatlist item with animation*/
import React, { Component } from 'react';
//Import React
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  UIManager,
  LayoutAnimation,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
//Import basic react native components
import Card from './Card';
//Import custom card component

console.disableYellowBox = true;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

  // LLamados de imagenes 
const imageUrl = 'https://vignette.wikia.nocookie.net/leagueoflegends/images/d/df/Ornn_OriginalSkin.jpg/revision/latest?cb=20170809014730';
const imageUr2 = 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/Riven_3.jpg';
const imageUr3 = 'http://ddragon.leagueoflegends.com/cdn/img/champion/splash/Jinx_0.jpg';
const imageUr4 = 'https://www.mobafire.com/images/champion/skins/landscape/kindred-shadowfire.jpg';
const error1 = 'https://img.freepik.com/vector-gratis/fondo-universo-error-404_23-2147748512.jpg?size=338&ext=jpg';


const cards = [
  {
    key: 0,
    uri: imageUrl,
    title: 'ORNN',
    description: 'Es Dios de la forja',
  },
  {
    key: 1,
    uri: imageUr2,
    title: 'RIVEN',
    description: 'La Forajida',
  },
  {
    key: 2,
    uri: imageUr3,
    title: 'JINX',
    description: 'La Asaltante',
  },
  {
    key: 3,
    uri: imageUr4,
    title: 'KINDRED',
    description: 'La Cazadora Nocturna',
  },
];

export default class MiLista extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards,
    };
  }
  
  setAnimation = () => {
    LayoutAnimation.configureNext({
      duration: 250,
      update: {
        type: LayoutAnimation.Types.easeIn,
        springDamping: 0.7,
      },
    });
    LayoutAnimation.configureNext({
      duration: 500,
      create: {
        type: LayoutAnimation.Types.easeIn,
        property: LayoutAnimation.Properties.scaleXY,
        springDamping: 0.7,
      },
    });
  };

  // Añadir lista 
  addItem = (() => {
    let key = cards.length;
    return () => {
      const { cards } = this.state;
      cards.unshift(
        {
        key,
        uri: error1,
        title: 'Heroes No Disponible ',
        description: 'Por favor remover el dato mostrada - No Disponible ',
        animated: true,

        
      }
  
      );
      this.setAnimation();
      this.setState({
        cards: cards.slice(0),
      });
      key++;
    };
  })();

  removeItem = key => {
    const { cards } = this.state;
    this.setAnimation();
    this.setState({
      cards: cards.slice().filter(card => card.key !== key),
    });
  };

  renderItem = ({ item }) => <Card item={item} removeItem={this.removeItem} />;

  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={styles.container}>
        <TouchableOpacity style={styles.addButton} onPress={this.addItem}>
            <Text style={styles.addIcon}>Mostrar Campeones</Text>
          </TouchableOpacity>
          <FlatList
            contentContainerStyle={styles.paragraph}
            data={this.state.cards}
            renderItem={this.renderItem}
            ItemSeparatorComponent={() => <View style={{ marginTop: 10 }} />}
            keyExtractor={item => item.key.toString()}
          />
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  // Fondo 
  container: {
    flex: 1,
    backgroundColor: '#c5cae9',
  },
  
  paragraph: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e',
    paddingBottom: 10,
  },

  // Color de Button
  addButton: {
    width: '100%',
    elevation: 3,
    backgroundColor: '#b71c1c',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 15
  },

  // Letra de Icono
  addIcon: {
    color: 'white',
    padding: 10,
    fontSize: 25,
    textAlign: 'center',
  },
});




