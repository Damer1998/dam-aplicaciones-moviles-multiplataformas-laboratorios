/*Example to add or remove flatlist item with animation*/
import React from 'react';
//Import React
import {
  View,
  StyleSheet,
  Text,
  Image,
  Animated,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
//Import basic react native components

export default class Card extends React.Component {
  render() {
    const { removeItem, item } = this.props;
    const { uri, title, description, key } = item;
    return (
      <Animated.View style={{ flex:1, alignItems: 'center', paddingVertical:5 }}>
        <TouchableOpacity
          onPress={() => removeItem(key)}
          style={styles.container}>
          <Image style={styles.thumbnail} source={{ uri }} />
          <View style={styles.metaDataContainer}>
            <View style={styles.metaDataContent}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.description}>{description}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  // Fondo
  container: {
    height: 80,
    elevation: 5,
    borderColor: '#b71c1c',
    borderRadius: 5,
    flexDirection: 'row',
    marginHorizontal: 25,
  },

  metaDataContainer: {
    flex: 1,
  },

  // Tamaño de la Imagen
  thumbnail: {
    width: 80,
    height: 80,
  },

  // Espacio de titulo y descripcion
  metaDataContent: {
    marginTop: 8,
    marginLeft: 25,
  },

  // Letra Titulo
  title: {
    color: '#1a237e',
    fontSize: 20,
    fontWeight: 'bold',
  },
  // Letra Descripcion  - Degradacion
  description: {
    fontSize: 16,
    color: '#263238',
    fontWeight: '800',
  },
});