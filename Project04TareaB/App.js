// Importaciones
import React, { Component } from 'react';

//Importaciones de  react-navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

// Importaciones de rutas de navegacion
import Lista from './pages/Lista';
import PrimeraActividad from './pages/Primera';
import SegundaActividad from './pages/Segunda';
import TerceraActividad from './pages/Tercera';
import CuartaActividad from './pages/Cuarto';
import QuintaActividad from './pages/Quinto';
import SextoActividad from './pages/Sexto';
import SeptimoActividad from './pages/Septimo';
import OctavoActividad from './pages/Octavo';

 // Atributos de Constancias
const App = createStackNavigator({
  Lista: { screen: Lista }, 

  PrimeraActividad: { screen: PrimeraActividad }, 
  SegundaActividad: { screen: SegundaActividad }, 
  TerceraActividad: { screen: TerceraActividad },
  CuartaActividad: { screen: CuartaActividad }, 
  QuintaActividad: { screen: QuintaActividad }, 
  SextoActividad: { screen: SextoActividad }, 
  SeptimoActividad: { screen: SeptimoActividad }, 
  OctavoActividad: { screen: OctavoActividad }, 

  },
  {
    initialRouteName: 'Lista',
  }
);
export default createAppContainer(App);