
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'http://1.bp.blogspot.com/_L78zEKWCMII/TNSRZm4OdAI/AAAAAAAABE8/VSIGs6oy_9U/s1600/Teemo_Splash_4.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'TEEMO',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Historia</Text>
      
              <Text style={styles.name} >Ornn valora su privacidad más que la mayoría de 
              semidioses. 
                Vive retirado bajo un antiguo volcán, donde forja objetos de calidad sin igual
                 en burbujeantes calderos de roca fundida.  </Text>
                 <Text style={styles.name} > Incluso desde estas profundidades,
                  Ornn percibe el peligro: los seres divinos se vuelven a entrometer. 
                  Para las guerras que se avecinan,necesitarán a un buen herrero </Text>      
            </View>
     
        <Button title="Retroceder" onPress={() => navigate('Lista')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#00838f",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
