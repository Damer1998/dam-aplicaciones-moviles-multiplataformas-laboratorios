//This is an example code for Bottom Navigation//
//Icons Used in this example is from local directory//
import React from 'react';
import { Button, Image } from 'react-native';

//Import React Navigation
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import HomeScreen from './pages/HomeScreen';
import SettingsScreen from './pages/SettingsScreen';

import DetalleScreen from './pages/DetalleScreen';

// LLamado de boton
import AlmacenadoScreen from './pages/AlmacenadoScreen';

const HomeStack = createStackNavigator(
  {
    //Defination of Navigaton from home screen
    Home: { screen: HomeScreen },


  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Home',
      //Header title
    },
  }
);

const SettingsStack = createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Settings: { screen: SettingsScreen },
    Almacenado: { screen: AlmacenadoScreen },
    
  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Envio',
      //Header title
    },
  }
);



const DetalleStack= createStackNavigator(
  {
    //Defination of Navigaton from setting screen
    Detalle: { screen: DetalleScreen },

  },
  {
    defaultNavigationOptions: {
      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Settings',
      //Header title
    },
  }
);




const App = createBottomTabNavigator(
  {
    //Defination of Navigaton bottom options
    Home: { screen: HomeStack },
    Settings: { screen: SettingsStack },
    Detalle: { screen: DetalleStack },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        if (routeName === 'Home') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/ListadoBlack.png')
                  : require('./asset/ListadoRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        } 
        
        else if (routeName === 'Detalle') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/VideoBlack.png')
                  : require('./asset/VideoRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        }
        
        else if (routeName === 'Settings') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/AjusteBlack.png')
                  : require('./asset/AjusteRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        }
      },
    }),
    tabBarOptions: {
      activeTintColor: '#42f44b',
      inactiveTintColor: 'gray',
    },
  }
);
export default createAppContainer(App);