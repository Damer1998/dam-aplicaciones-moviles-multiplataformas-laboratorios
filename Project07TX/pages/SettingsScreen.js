//Area de Ajuste
import React from 'react';

import { 
    StyleSheet , 
    Text, 
    View,
    TextInput,
    Button ,
    KeyboardAvoidingView,
    TouchableOpacity } from 'react-native';

import {Hoshi} from 'react-native-textinput-effects'


 import axios from 'axios';

export default class SettingsScreen extends React.Component {

    constructor(props){
        super(props);
      
        this.state = {
            name:''
      
        };
      
      }
      
  

      // Borra todo el contenido
      clear = () => {
          this.textInputRef.clear();
      }
      
      
      
      
      render(){
          
      
        return (
          <View style={styles.container}>
      
            <Hoshi
             ref={ref => this.textInputRef = ref}
            label={"Ingresar Nombre"}
            value={this.state.name}
            onChangeText={name => this.setState({name})}
            keyboardType ='numeric'
            />

           <Text>{this.state.name}</Text>

           <TouchableOpacity style = {styles.borrar} onPress={this.clear}>
                 <Text style = {styles.Textosinlabor}> Borrar Datos </Text>
               </TouchableOpacity>


               <TouchableOpacity
            style = {styles.borrar}
            onPress={() => this.props.navigation.navigate('Almacenado')}>
            <Text style = {styles.Textosinlabor} >Ingresar</Text>
          </TouchableOpacity>
      

          </View>
      
        );
      }
      }
      
      const styles = StyleSheet.create({
      
        container:{
      justifyContent: 'center',
      paddingHorizontal:0,
      
        },
      
       
      
      text: {
        alignItems : 'center' ,
        padding:10,
      
      
      },
      text1: {
          alignItems : 'center' ,
          padding:10,
          color : '#1a237e',
          fontSize: 25,
        },
      
      ingresaedad: {
          marginTop: 8,
          marginLeft: 25,
          height : 50 ,
          width : 300,
          borderColor: '#1a237e' ,
          borderWidth: 1.5,
         
        },
      
      button: {
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
        width: 300,
        marginTop: 16,
      
      },
      
      countContainer :{
      alignItems : 'center',
      padding : 10,
      },
      
      Textosinlabor: {
          fontSize: 20,
          alignItems : 'center',
          color:'white',
          padding : 5,
        
        },
      
        borrar: {
          backgroundColor : '#b71c1c' ,
          fontSize: 25,
          alignItems : 'center',
          height : 50 ,
          width : 300,
          padding : 10,
          marginTop: 8,
          marginLeft: 25,
        },
      
      
        enviar: {
          backgroundColor : '#1a237e' ,
          fontSize: 25,
          alignItems : 'center',
          height : 50 ,
          width : 300,
          padding : 10,
          marginTop: 8,
          marginLeft: 25,
        },
      
      
      countText : {
        color : 'blue',
      }
      
      });
      
       