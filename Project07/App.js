 // Importaciones de ruta
 
 import TransferenceFirst from './app/components/transferences/TransferenceFirst';
 import TransferenceSecond from './app/components/transferences/TransferenceSecond';
 import TransferenceThird from './app/components/transferences/TransferenceThird';


 import {NavigationContainer, TabActions} from '@react-navigation/native';
 import {createStackNavigator} from '@react-navigation/stack';
 import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

 const TransferenceStack = createStackNavigator();


<Tab.Screen
  name="Transference"
  component={TransferenceStackScreen}
  options={{
    tabBarLabel: 'Transference',
    tabBarIcon: ({color, size}) => (
      <MaterialCommunityIcons name="home" color={color} size={size}/>
    ),

  }}

/>

  function TransferenceStackScreen(){
   return(
     <TransferenceStack.Navigator>
       <TransferenceStack.Screen name="First" component={TransferenceFirst}/>
       <TransferenceStack.Screen name="Second" component={TransferenceSecond}/>
       <TransferenceStack.Screen name="Third" component={TransferenceThird}/>


     </TransferenceStack.Navigator>




   )
 }