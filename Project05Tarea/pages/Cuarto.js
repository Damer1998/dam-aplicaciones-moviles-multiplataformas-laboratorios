
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://www.pcgamesn.com/wp-content/uploads/2018/09/lol-garen.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'GAREN',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Historia</Text>
      
              <Text style={styles.name} >Como un orgulloso y noble guerrero, Garen
               forma parte de la Vanguardia Valerosa. Es popular entre sus compañeros
                y bastante respetado por sus enemigos.  </Text>
                 <Text style={styles.name} >Es descendiente de la prestigiosa
                  familia Guardia de la Corona  </Text>      
            </View>
     
        <Button title="Retroceder" onPress={() => navigate('Lista')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#ff8f00",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
