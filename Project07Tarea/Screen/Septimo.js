
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://gadeaciencia.org/wp-content/uploads/2017/05/JuanCarlosIzpisuaBelmonte1-e1495465488848.jpg';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'Juan Carlos Izpisúa',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Obras</Text>
      
              <Text style={styles.name} >Juan Carlos Izpisúa Belmonte, conocido como Juan Carlos Izpisúa,
               es un farmacéutico español, especializado en el campo de 
               la biología del desarrollo. </Text>
                 <Text style={styles.name} > Especialista em medicamentos y tranquilizantes </Text>      
            </View>
     
        <Button title="Retroceder" onPress={() => navigate('SettingsScreen')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#2e7d32",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
