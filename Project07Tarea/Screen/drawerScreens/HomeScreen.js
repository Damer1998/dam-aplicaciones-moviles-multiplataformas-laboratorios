
import React from 'react';

//Import all required component
import { StyleSheet, 
  Text,
   View, 
   FlatList,
    Image, 
    Button,
    TouchableOpacity } from 'react-native';


    // Lista de actividades
const direuno = 'PrimeraActividad';
const diredos = 'SegundaActividad';
const diretres = 'TerceraActividad';
const direcuatro = 'CuartaActividad';
const direcinco = 'QuintaActividad';
const direseis = 'SextoActividad';


const data =[
  {
    key: "0",
    name: "Anestesico Bupivacaina",
    tipo: "N.D",
    enlace: direuno,
    descripcion: "Precio = 30 soles",
    photo: "https://www.ecured.cu/images/5/5a/Bupivaca%C3%ADna_clorhidrato.jpg"
},
{
     key: "1",
    name: "Antisecretorio Butropina",
    tipo: "D",
    enlace: diredos,
    descripcion: "Precio = 30 soles",
    photo: "https://ve.tumedico.com/assets/img/medicinas/vad.jpg"
},
{
     key: "2",
    name: "Ibuprofeno",
    tipo: "D",
    enlace: diretres,
    descripcion: "Precio = 40 soles",
    photo: "https://www.newtral.es/wp-content/uploads/2020/03/ibuprofeno.jpg"
},
{
   key: "3",          
    name: "Morfina",
    tipo: "N.D",
    enlace: direcuatro,
    descripcion: "Precio = 80 soles",
    photo: "https://assets.metrolatam.com/cl/2016/05/03/screen-shot-2016-05-03-at-1-56-24-pm-1-1200x800.jpg"
},
{
  key: "4",          
    name: "Pastilla loperamida",
    tipo: "D",
    enlace: direcinco,
    descripcion: "Precio = 30 soles",
    photo: "https://www.ecured.cu/images/5/5a/Bupivaca%C3%ADna_clorhidrato.jpg"
},
{
  key: "5",         
    name: "Pastilla rasecadotril",
    tipo: "D",
    enlace: direseis,
    descripcion: "Precio = 10 soles",
    photo: "https://www.soin-et-nature.com/24042-menu_default/racecadotrilo-100-mg-biogaran-consejo-diarrea-10-capsulas.jpg"
}
];

//HomeScreen



export default class HomeScreen extends React.Component {
  
  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }


  render(){
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <FlatList
          style={{flex:1}}
          data={this.state.data}


          renderItem={({ item }) => 
          <TouchableOpacity style={styles.listItem} 
    
          onPress={() => navigate(item.enlace)} >


            <Image source={{uri:item.photo}}  style={{width:60, height:60,borderRadius:40}} />
            <View style={{alignItems:"center",flex:1}}>
              <Text style={styles.name}>{item.name}</Text>
              <Text style={styles.de}>{item.descripcion}</Text>
            </View>

            <View style={{alignItems:"center"}}>
              <Text style={styles.name}>VENTA</Text>
              <Text style={styles.de}>{item.tipo}</Text>
            </View>  


      
      
          </TouchableOpacity>
        
  
        }


          keyExtractor={item =>
            item.key}





        />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5cae9',

  },
  de: {
    color: '#fafafa'

  },


  name: {
    flex: 0.5,
    color: '#fafafa',
    fontWeight:"bold"
  },

  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#e53935",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
