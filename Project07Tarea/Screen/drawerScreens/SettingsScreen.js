
import React from 'react';

//Import all required component
import { StyleSheet, 
  Text,
   View, 
   FlatList,
    Image, 
    Button,
    TouchableOpacity } from 'react-native';


    // Lista de actividades

const diresiete = 'SeptimoActividad';
const direocho = 'OctavoActividad';

const data =[

{
  key: "0",         
    name: "Juan Carlos Izpisúa",
    tipo: "Pediatra",
    enlace: diresiete,
    descripcion: "Telefono:952874274",
    photo: "https://gadeaciencia.org/wp-content/uploads/2017/05/JuanCarlosIzpisuaBelmonte1-e1495465488848.jpg"
},
{
  key: "1",          
    name: "Tasuku Honjo",
    tipo: "Farmacologo",
    enlace: direocho,
    descripcion: "Telefono:987456321",
    photo: "https://upload.wikimedia.org/wikipedia/commons/b/b3/Tasuku_Honjo_201311.jpg"
}
];

//HomeScreen



export default class HomeScreen extends React.Component {
  
  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }


  render(){
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <FlatList
          style={{flex:1}}
          data={this.state.data}


          renderItem={({ item }) => 
          <TouchableOpacity style={styles.listItem} 
    
          onPress={() => navigate(item.enlace)} >


            <Image source={{uri:item.photo}}  style={{width:60, height:60,borderRadius:40}} />
            <View style={{alignItems:"center",flex:1}}>
              <Text style={styles.name}>{item.name}</Text>
              <Text style={styles.de}>{item.descripcion}</Text>
            </View>

            <View style={{alignItems:"center"}}>
              <Text style={styles.name}>Cargo</Text>
              <Text style={styles.de}>{item.tipo}</Text>
            </View>  


      
      
          </TouchableOpacity>
        
  
        }


          keyExtractor={item =>
            item.key}





        />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5cae9',

  },
  de: {
    color: '#fafafa'

  },


  name: {
    flex: 0.5,
    color: '#fafafa',
    fontWeight:"bold"
  },

  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#e53935",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
