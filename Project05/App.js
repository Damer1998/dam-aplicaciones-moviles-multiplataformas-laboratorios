import React , {Component} from 'react';
import { StyleSheet ,
   Text,
    View,
    TextInput, 
   Alert,} from 'react-native';


import OurFlatList from './app/components/ourFlatList/OurFlatList'

export default class App extends Component {

showAlert = () => {
  Alert.alert(
    'Titulo',
    'Mensaje',
  [
    {
    text:'Cancel',
    onPress: () => console.log('Presione para Cancelar'),
    style:'cancel',
  },
    {
      text: 'OK', onPress: () => console.log('Presiones okey')
    },
],
{cancelable:false},
    );
};

render(){
  return  <OurFlatList showAlert={this.showAlert}/>;
}


}

const styles = StyleSheet.create({

  container:{
flex:1,
justifyContent: 'center',
paddingHorizontal:10,


  }

});

 