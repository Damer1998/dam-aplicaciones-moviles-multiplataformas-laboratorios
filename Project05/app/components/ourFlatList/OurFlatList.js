import React  from 'react';
import {View,
    Text,
    TouchableOpacity , 
    FlatList,
    StyleSheet } from 'react-native';


const DATA = [
{
  id:'1',
  title: 'Primer Item',

},
{
  id:'2',
  title: 'Segundo Item'
},
{
  id:'3',
  title: 'Tercer Item'

},
]

// Modificado en Lab 5
function Item({title , showAlert}) {
    return(
      <TouchableOpacity onPress= {showAlert}>
      <View style={styles.item}>
          <Text style={styles.title}>{title}</Text>
      </View>  
      </TouchableOpacity>
    );
    
}

//Modificado en la 5
const ListEmpty = () =>{
return (
  <View  >
    <Text style={{textAlign: 'center'}}>No Data Found</Text>
  </View>
);
};

/*
const OurFlatList = props => (
    
  <View style={styles.container}>
      <FlatList  
      data={DATA}
      renderItem={({item}) => ( 
      <Item title={item.title} showAlert={props.showAlert}/> )}
      keyExtractor = {item => item.id}
      ListEmptyComponent = {ListEmpty}
      />
      
  </View>
);*/

export default function OurFlatList() {
    return (
            <View style={styles.container}>
                <FlatList  
                data={DATA}
                renderItem={({item}) => 
                <Item title={item.title}/>}
                keyExtractor = {item => item.id}
                />
                
            </View>
    );
    
}

const styles = StyleSheet.create({

    container:{
  flex:1,
marginTop:20,
  
    },

  
  item: {
    marginHorizontal: 16,
    marginVertical : 8,
    backgroundColor : 'green' ,
    padding : 20,
    
  
  
  },
  
  
  title: {
    
    fontSize:32,
  }
  

  
  });
  
   