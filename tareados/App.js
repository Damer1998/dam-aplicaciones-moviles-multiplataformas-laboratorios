import React, { Component } from 'react';

import {
  
  StyleSheet,
  Text,
  View,
  Image,
  Button,
} from 'react-native';


import LoginForm from './LoginForm';


export default class App extends Component {



render(){
  return (
    <View style={styles.container}>
      <View style = {styles.logoContainer}>


      <Image 
      
      style={styles.logo}
      source={require('./img/fondo.png') }
      />

<Text style = {styles.title}>
Bienvenido Usuario

</Text>
  </View>


    
      <View style = {styles.parteinferior}>

      <LoginForm/>


      </View>
    </View>
  );
}
}

const styles = StyleSheet.create({


  // Fondo de color de la Aplicacion
container:{

  flex : 1,
  backgroundColor : 'gray'

},

// Imagen logo.png posicion  
// justifyContent : 'center'
logoContainer: {
  alignItems : 'center',
  flexGrow : 0.4 

},

logo: {

  width : 360,
  height : 150

},


parteinferior: {
  flex: 10,
  backgroundColor: '#e3aa1a'
},


// Estilo del Titulo Color Espacio Espacio
title :{
  color : '#FFF',
  marginTop : 10,
  width : 300 ,
  textAlign : 'center',
  opacity : 0.9 ,
  fontSize: 30


}



});




