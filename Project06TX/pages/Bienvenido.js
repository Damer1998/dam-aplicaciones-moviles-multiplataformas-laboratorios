// Centro de Control para la navigation Button
import React from 'react';
//import react in our code.
import { Text, View, Button, Image } from 'react-native';


//Importaciones de React Navigation
import {createAppContainer} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';

import ListadoScreen from './ListadoScreen';
import AjusteScreen from './AjusteScreen';
// Nuevo Ingreso
import VideosScreen from './VideosScreen'


// Bienvenido Boton Navigator
const Bienvenido = createBottomTabNavigator(
  {
    //Definicion de navegacion de boton de opciones
    Hogar: { screen: ListadoScreen },
   
    Video: {screen:VideosScreen},
    Ajuste: { screen: AjusteScreen },
  },
  {
    // Se establecio la condicion
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;


        if (routeName === 'Hogar') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/ListadoBlack.png')
                  : require('./asset/ListadoRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        } 
        
        
        
        else if (routeName === 'Video') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/VideoBlack.png')
                  : require('./asset/VideoRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        }

        else if (routeName === 'Ajuste') {
          return (
            <Image
              source={
                focused
                  ? require('./asset/AjusteBlack.png')
                  : require('./asset/AjusteRed.png')
              }
              style={{
                width: 20,
                height: 20,
                borderRadius: 40 / 2,
              }}
            />
          );
        }



      },
    }),
    tabBarOptions: {
      activeTintColor: '#4e342e',
      inactiveTintColor: '#1a237e',
    },
  }
);


export default createAppContainer(Bienvenido);

