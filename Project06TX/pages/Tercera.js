
import React, { Component } from 'react';

import { StyleSheet, View, Text, 
  TouchableOpacity,
  Image,  Button } from 'react-native';

  const imageUrl = 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/fc357b30-6b5c-48f0-b7a7-ce8f48ae8010/dciv6xo-6dc052cd-0833-42bf-b664-2f12796f9ca6.png/v1/fill/w_1024,h_663,q_80,strp/kindred_human_by_draconli_dciv6xo-fullview.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOiIsImlzcyI6InVybjphcHA6Iiwib2JqIjpbW3siaGVpZ2h0IjoiPD02NjMiLCJwYXRoIjoiXC9mXC9mYzM1N2IzMC02YjVjLTQ4ZjAtYjdhNy1jZThmNDhhZTgwMTBcL2RjaXY2eG8tNmRjMDUyY2QtMDgzMy00MmJmLWI2NjQtMmYxMjc5NmY5Y2E2LnBuZyIsIndpZHRoIjoiPD0xMDI0In1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmltYWdlLm9wZXJhdGlvbnMiXX0.T9qfG2sKH2cvTTatfIj52l6KnYTs2DE9e-b-i09vh3A';

export default class Segunda extends Component {
  constructor(props) {
    super(props);
    this.state = {

      
    };
    

  }
  static navigationOptions = {
    title: 'KINDRED',
    
  };


  render() {
    const { navigate } = this.props.navigation;
    return (

      <View style={styles.listItem}  >

      <View style={styles.container}>
      <Image source={{uri:imageUrl}}  style={{width:300, height:200,borderRadius:20}} />

            <View style={{alignItems:"center",flex:1}}>
            
            <Text></Text>
              <Text style={styles.titulo}>Historia</Text>
      
              <Text style={styles.name} >Kindred es el abrazo blanco de la nada y
               el crujir de dientes en la oscuridad. Pastor y carnicero, poeta y
                primitivo, son uno y ambos.
                 </Text>
              
              <Text style={styles.name} > Separados, pero nunca divididos, los
               Kindred representan las esencias hermanas de la
                muerte. Oveja ofrece una vía de escape rápida para los mortales que 
                acepten su destino
              
              
              </Text>      
            </View>
     
        <Button title="Retroceder" onPress={() => navigate('Bienvenido')} />
      </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

 

  titulo: {
    flex: 0.1,
    color: '#fafafa',
    fontWeight:"bold"
  },

  name: {
    flex: 0.3,
    color: '#fafafa'
  
  },



  listItem:{
    margin:12,
    padding:10,
    backgroundColor:"#546e7a",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
