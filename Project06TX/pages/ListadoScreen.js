//Entorno de Boton Navigation Screen
import React from 'react';
//Importacion de herramientas
import { StyleSheet, 
  Text,
   View, 
   FlatList,
    Image, 
    Button,
    TouchableOpacity } from 'react-native';

// Lista de actividades
const direuno = 'PrimeraActividad';
const diredos = 'SegundaActividad';
const diretres = 'TerceraActividad';
const direcuatro = 'CuartaActividad';
const direcinco = 'QuintaActividad';
const direseis = 'SextoActividad';
const diresiete = 'SeptimoActividad';
const direocho = 'OctavoActividad';





const data =[
  {
    key: "0",
    name: "ORNN",
    tipo: "H",
    enlace: direuno,
    descripcion: "El semidios de la Forja",
    photo: "https://i.ytimg.com/vi/XMiKW3_E-Fs/maxresdefault.jpg"
},
{
     key: "1",
    name: "RIVEN",
    tipo: "E",
    enlace: diredos,
    descripcion: "La olvidada",
    photo: "https://am-a.akamaihd.net/image?f=https://news-a.akamaihd.net/public/images/pages/2018/may/pulsefire2018/img/pfr-wp.jpg"
},
{
     key: "2",
    name: "KINDRED",
    tipo: "C",
    enlace: diretres,
    descripcion: "La cazadora",
    photo: "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/Kindred_0.jpg"
},
{
   key: "3",          
    name: "GAREN",
    tipo: "C",
    enlace: direcuatro,
    descripcion: "El caballero de Demacia",
    photo: "https://th.bing.com/th/id/OIP.bn3DiJ7XzLgjjxhD9ah8HAHaEr?pid=Api&rs=1"
},
{
  key: "4",          
    name: "DARIUS",
    tipo: "C",
    enlace: direcinco,
    descripcion: "El caballero de Noxus",
    photo: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/4cb531bc-c606-411b-8e3f-8f95895d1829/d5u4v9f-4253d3da-bc5d-4dc8-ab58-3a8e91381707.jpg/v1/fill/w_1024,h_750,q_75,strp/darius___league_of_legends_by_thomaswievegg-d5u4v9f.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwic3ViIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsImF1ZCI6WyJ1cm46c2VydmljZTppbWFnZS5vcGVyYXRpb25zIl0sIm9iaiI6W1t7InBhdGgiOiIvZi80Y2I1MzFiYy1jNjA2LTQxMWItOGUzZi04Zjk1ODk1ZDE4MjkvZDV1NHY5Zi00MjUzZDNkYS1iYzVkLTRkYzgtYWI1OC0zYThlOTEzODE3MDcuanBnIiwid2lkdGgiOiI8PTEwMjQiLCJoZWlnaHQiOiI8PTc1MCJ9XV19.j60D6JEksfFARX7kyC3oPtiKlqmnmy5vonYmj2-QQHs"
},
{
  key: "5",         
    name: "TEEMO",
    tipo: "T",
    enlace: direseis,
    descripcion: "El explorador",
    photo: "https://fb.ru/misc/i/gallery/28277/1404140.jpg"
},
{
  key: "6",         
    name: "JARVAN IV",
    tipo: "E",
    enlace: diresiete,
    descripcion: "El rey de Demacia",
    photo: "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/JarvanIV_0.jpg"
},
{
  key: "7",          
    name: "VEIGAR",
    tipo: "M",
    enlace: direocho,
    descripcion: "El hechicero",
    photo: "https://vignette.wikia.nocookie.net/leagueoflegends/images/e/e8/Veigar_OriginalCentered.jpg/revision/latest?cb=20180414203657"
}
];



export default class ListadoScreen extends React.Component {
  
  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }


  render(){
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <FlatList
          style={{flex:1}}
          data={this.state.data}


          renderItem={({ item }) => 
          <TouchableOpacity style={styles.listItem} 
    
          onPress={() => navigate(item.enlace)} >


            <Image source={{uri:item.photo}}  style={{width:60, height:60,borderRadius:40}} />
            <View style={{alignItems:"center",flex:1}}>
              <Text style={styles.name}>{item.name}</Text>
              <Text style={styles.de}>{item.descripcion}</Text>
            </View>

            <View style={{alignItems:"center"}}>
              <Text style={styles.name}>TIPO</Text>
              <Text style={styles.de}>{item.tipo}</Text>
            </View>  


      
      
          </TouchableOpacity>
        
  
        }


          keyExtractor={item =>
            item.key}





        />
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c5cae9',

  },
  de: {
    color: '#fafafa'

  },


  name: {
    flex: 0.5,
    color: '#fafafa',
    fontWeight:"bold"
  },

  listItem:{
    margin:10,
    padding:10,
    backgroundColor:"#e53935",
    width:"90%",
    flex:1,
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:5
  }
});
