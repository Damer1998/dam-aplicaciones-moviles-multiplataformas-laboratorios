// Boton Navegator AjusteScren
import React from 'react';

/// Importaciones Basicas
import { StyleSheet, 
  Text,
   View, 
   FlatList,
    Image, 
    Button,
    ImageBackground,
    TouchableOpacity ,
    Dimensions} from 'react-native';

    import { TextInput } from 'react-native-gesture-handler';

    
  const fondoLoguin = 'https://images.vexels.com/media/users/3/72688/raw/a7c4b20e9bf9c64ce9b455a5f2e371a9-fondo-de-estrellas-del-espacio.jpg';

    const{width: WIDTH} = Dimensions.get('window')
const data =[
  {
    key: "0",
    name: "     Usuario",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://cdn3.iconfinder.com/data/icons/inficons-set-2/512/648969-star-ratings-512.png"
},
{
     key: "1",
    name: "     Notificaciones",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://masteres.slke.org/images/icono_menu_usuario_notificaciones_master-universitario-slke-uemc.png"
},
{
     key: "2",
    name: "     Apariencias",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://images.vexels.com/media/users/3/135899/isolated/preview/9d55a1dc7fbba0b7e8873e21b7135626-ilustraci-n-de-icono-de-ojo-by-vexels.png"
},
{
   key: "3",          
    name: "     Privacidad & Seguridad",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://kiof.net/images/kioflock.png"
},
{
  key: "4",          
    name: "     Ayuda y Soporte",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://www.tccd.edu/magazine/assets/images/volume-02/issue-02/customer-care/customer-headset.png"
},
{
  key: "5",         
    name: "     Acerca de ",
    tipo : "http://4starseed.com/graphics/arrow.png",
    photo: "https://img2.freepng.es/20180320/krq/kisspng-help-desk-computer-icons-business-technical-suppor-help-desk-icon-png-5ab0a88e215e38.4140330815215269261367.jpg"
}
];

export default class AjusteScren extends React.Component {
  // Se llama el const de data
  constructor(props) {
    super(props);
    this.state = {
      data,
    };
  }
  render() {
    return (
      <ImageBackground  source={{uri:fondoLoguin}} style={styles.fondo}>
      <View style={styles.container}>
<View>
  <Text style={styles.deTitulo}>Centro de Ajustes</Text>
</View>
<View style={styles.espacio}>
    

    <TextInput
    style={styles.elinput}
    placeholder={'Ingreso de Busqueda'}
    placeholderTextColor={'rgba(255,255,255,0.7)'}
    underlineColorAndroid='transparent'
    
    />
 </View>

      <FlatList

        data={this.state.data}


        renderItem={({ item }) => 
        <TouchableOpacity style={styles.listItem} >
  
     
          <Image source={{uri:item.photo}}  style={{width:30, height:30,borderRadius:40}} />
          <View style={{flex:2.6}}>
            <Text style={styles.de}>{item.name}</Text>
         
          </View>

          <View style={{alignItems:"center",flex:0.4}}>
          <Image source={{uri:item.tipo}}  style={{width:20, height:20,borderRadius:40}} /> 
            
            </View>  

    
    
        </TouchableOpacity>
      

      }


        keyExtractor={item =>
          item.key}





      />
      
    </View>
    </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({

fondo:{
  marginTop: 50, fontSize: 25
},


  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
  
  container: {
    flex: 1,
    backgroundColor: '#e0e0e0',
    margin: 20,
    
  },

  de: {
    color: '#303f9f',
   alignItems: 'center',
  
  },

  deTitulo:{
color:'#c62828',
margin: 20,
alignSelf:"center",
fontSize: 25
  },


  name: {

    color: 'black',
    fontWeight:"bold"
  },

  
  elinput: {
    width: WIDTH - 100,
    height: 40,
    borderRadius:25,
    fontSize:16,
    paddingLeft:40,
    backgroundColor:'rgba(20,2,20,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal:25,
   

  },
  espacio: {
    flex: 1.0,


  },
  fondo: {
    flex: 1,
    width: null,
    height:null,
    justifyContent:'center',
    alignItems: 'center',


  },


  //Color
  listItem:{
    margin:2,
    padding:2,
    backgroundColor:"transparent",
    width:"90%",
   
    alignSelf:"center",
    flexDirection:"row",
    borderRadius:4
  }
});

