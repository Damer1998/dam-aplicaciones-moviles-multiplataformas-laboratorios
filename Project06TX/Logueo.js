import React, { Component } from 'react';
//import react in our code.
import { StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Button, 
  ImageBackground,
  Dimensions} from 'react-native';


// Contraseña de Logueo
const userInfo = {
  username: '1',
  password: '2'
}

// Importacion de Text Input    
import { TextInput } from 'react-native-gesture-handler';



const{width: WIDTH} = Dimensions.get('window')

  const imageUrl = 'https://images4.alphacoders.com/659/thumb-1920-659466.jpg';
  const logo = 'https://i.pinimg.com/originals/44/0c/01/440c013fd6ac8ef97ca73d08741d2444.png';

export default class Primera extends Component {
  constructor(props) {
    super(props);
    this.state = {
            username:'',
            password:''
      
    };
  }
  static navigationOptions = {
    title: 'Iniciar Sesion',
    
  };




  render() {
    const { navigate } = this.props.navigation;
    return (

      

            <ImageBackground  source={{uri:imageUrl}} style={styles.fondo}>
                <View style={styles.container}>
                <Image source={{uri:logo}}  style={styles.logo} />
                <Text style={styles.LogoText}>Bienvenido</Text>
                </View>

                <View style={styles.espacio}>
    

                    <TextInput
                    style={styles.elinput}
                    placeholder={'Usuarios'}
                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                    underlineColorAndroid='transparent'
                    
                    // Prueba Nro25
                    onChangeText={(username) =>
                    this.setState({username})}
                    value={this.state.username}
                    autoCapitalize= "none"
                    />
                 </View>

                 <View  style={styles.espacio}>

                    <TextInput

                    style={styles.elinput}
                    placeholder={'Ingresar Contraseña'}
                    secureTextEntry={true}
                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                    underlineColorAndroid='transparent'

                    // Prueba Nro25
                    onChangeText={(password) =>
                     this.setState({password})}
                     value={this.state.password}                    
                    />
                </View>

                 <View  style={styles.espacioboton}>

                
                <Button 
                title="Ingresar"
                onPress={this._login}  
                // onPress={() => navigate('Lista')} 
                
                />
                </View>
            
            </ImageBackground>
           
         
//Boton Original <Button title="Ingresar" onPress={() => navigate('Lista')} />

    );
  }

// Prueba N25

_login = async() => {
    if(userInfo.username === this.state.username && userInfo.password ===  this.state.password){
       alert('Ingresando a Lista');
      this.props.navigation.navigate('Bienvenido')
    }else{
        alert('Por favor ingrese los Datos Correctos ')
     
    }
}

}

const styles = StyleSheet.create({
  container: {
    flex: 0.5,


  },

  inputIcon: {
    position: 'absolute',
    top:4,
    left:45,


  },


  espacio: {
    flex: 0.14,


  },

  espacioboton: {
    flex: 0.14,
    width: null,
    height:null,

  },


  fondo: {
    flex: 1,
    width: null,
    height:null,
    justifyContent:'center',
    alignItems: 'center',


  },


  logoContainer: {
    alignItems:'center'    
  },
  logo: {
    width:250,
    height:170,
    
  },
  LogoText: {
      
    color:'white',
    fontSize:50,
    fontWeight: '800',
    marginTop:10,
    opacity:0.5,
  },



  elinput: {
    width: WIDTH - 55,
    height: 45,
    borderRadius:25,
    fontSize:16,
    paddingLeft:45,
    backgroundColor:'rgba(0,0,0,0.35)',
    color: 'rgba(255,255,255,0.7)',
    marginHorizontal:25,
   

  },

  
});
