// Centro Controlador
import React, { Component } from 'react';

// Importaciones de React-Navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';


// Importacion de ruta de Logueo
import Logueo from './Logueo';

// Importaciones de rutas de navegacion
import Bienvenido from './pages/Bienvenido';

import PrimeraActividad from './pages/Primera';
import SegundaActividad from './pages/Segunda';
import TerceraActividad from './pages/Tercera';
import CuartaActividad from './pages/Cuarto';
import QuintaActividad from './pages/Quinto';
import SextoActividad from './pages/Sexto';
import SeptimoActividad from './pages/Septimo';
import OctavoActividad from './pages/Octavo';







 // Atributos de Constancias
 const App = createStackNavigator({

  Logueo: { screen: Logueo }, 
  Bienvenido: { screen: Bienvenido }, 

  
  PrimeraActividad: { screen: PrimeraActividad }, 
  SegundaActividad: { screen: SegundaActividad }, 
  TerceraActividad: { screen: TerceraActividad },
  CuartaActividad: { screen: CuartaActividad }, 
  QuintaActividad: { screen: QuintaActividad }, 
  SextoActividad: { screen: SextoActividad }, 
  SeptimoActividad: { screen: SeptimoActividad }, 
  OctavoActividad: { screen: OctavoActividad }, 

  },
  {
    // Js Exterior Iniciar
    initialRouteName: 'Logueo',
  }
);
export default createAppContainer(App);