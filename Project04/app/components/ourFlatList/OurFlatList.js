import React  from 'react';
import {View,
    Text,
    TouchableOpacity , 
    FlatList,
    StyleSheet } from 'react-native';


const DATA = [
{
  id:'1',
  title: 'Primer Item',

},
{
  id:'2',
  title: 'Segundo Item'
},
{
  id:'3',
  title: 'Tercer Item'

},
]


function Item({title}) {
    return(
      <View style={styles.container}>
          <Text style={styles.item}>{title}</Text>
      </View>  
    );
    
}


export default function OurFlatList() {
    return (
            <View style={styles.container}>
                <FlatList  
                data={DATA}
                renderItem={({item}) => <Item title={item.title}/>}
                keyExtractor = {item => item.id}
                />
                
            </View>
    );
    
}

const styles = StyleSheet.create({

    container:{
  flex:1,
marginTop:20,
  
    },

  
  item: {
    marginHorizontal: 16,
    marginVertical : 8,
    backgroundColor : 'green' ,
    padding : 20,
    
  
  
  },
  
  
  title: {
    
    fontSize:32,
  }
  

  
  });
  
   