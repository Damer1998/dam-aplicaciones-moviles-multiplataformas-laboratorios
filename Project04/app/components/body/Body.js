import React , {Component} from 'react';

import {View,Text,TouchableOpacity , StyleSheet } from 'react-native';

const Body =  props => (

    <View>
        <Text>{props.textBody}</Text>
        <TouchableOpacity style={styles.button} onPress={props.onBodyPress}>
            <Text>Touch Aqui -- Body.js</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({

  button:{

  top:10,
  alignItems: 'center',
  backgroundColor: '#8bc34a',
  padding: 10,
  
    },
});


export default Body;