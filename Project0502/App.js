// No funciona esta weba
/*
import React, { Component } from 'react';

import { StyleSheet,
  View,
  Text, 
  TouchableOpacity,
  Image,  
  Button } from 'react-native';

// Importaciones 
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

// Componentes HomeoScreen 
function HomeScreen({navigation}) {
  return (
<View style={{flex:1 , alignItems: 'center' , justifyContent: 'center'}}>
  <Text>
    Bienvenido A HomeoScreen
  </Text>
  <Button
  title="Ingresar a la otra actividad"
  onPress={()=> navigation.navigate('Details')}
  />
</View>
  ); 
}

// Componentes DetailsScreen
function DetailsScreen({navigation}) {
  return (
<View style={{flex:1 , alignItems: 'center' , justifyContent: 'center'}}>
  <Text>
    Bienvenido A DetailsScreen
  </Text>
  <Button
  title="Ingresar a la otra actividad"
  onPress={()=> navigation.navigate('Home')}
  
  />

  
</View>


  );
  
}


export default class App extends Component {


  render() {
  
    return (

  <NavigationContainer>
    <Stack.Navigator>
      <Stack.Screen name ="Home" component={HomeScreen}/>

      <Stack.Scree name ="Details" component={DetailsScreen}/>
    </Stack.Navigator>
  </NavigationContainer>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  }

 
});
*/

//Example to Hide Navigation Bar in React Native on Click of a Button//
import React, { Component } from 'react';
//import react in our code. 

//Import react-navigation
import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';
 
import FirstPage from './pages/FirstPage';
//import all the screens we are going to use
 
const App = createStackNavigator({
    FirstPage: { screen: FirstPage }, 
  },
  {
    initialRouteName: 'FirstPage',
  }
);
export default createAppContainer(App);