
import React, { Component } from 'react';

import { StyleSheet, View, Button, Text } from 'react-native';


export default class FirstPage extends Component {
  constructor(props) {
    super(props);

    this.isVisible = true;
  }

  static navigationOptions = ({ navigation }) => {
    if (navigation.state.params && !navigation.state.params.fullscreen) {
   
      return { header: null };
    } else {
 
      return {
        title: navigation.getParam('Title', 'Details'),
        headerStyle: {
          backgroundColor: navigation.getParam('BackgroundColor', 'white'),
        },
        headerTintColor: navigation.getParam('HeaderTintColor', 'black'),
      };
    }
  };

  onFullScreen() {
    // Set the params to pass in fullscreen isVisible to navigationOptions
    this.props.navigation.setParams({
      fullscreen: !this.isVisible,
    });
    this.isVisible = !this.isVisible;
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Text style={{ textAlign: 'center', fontSize: 18, margin: 16 }}>
        Bienvenido A DetailsScreen
        </Text>
        <Button
          title={this.isVisible ? 'Ingresar a la otra actividad' : 'Ingresar a la otra actividad'}
          onPress={() => this.onFullScreen()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});